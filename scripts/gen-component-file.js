const { resolve, join } = require('path');
const fs = require('fs');
const fg = require('fast-glob');

// fg
// src/**/*.js — matches all files in the src directory (any level of nesting) that have the .js extension.
// src/*.?? — matches all files in the src directory (only first level of nesting) that have a two-character extension.
// file-[01].js — matches files: file-0.js, file-1.js.

const COMPONENT_ROOT = resolve(__dirname, '../src');

// // 路径是否存在
function exist(path) {
  return fs.existsSync(path);
}

function generatorMkdirSync(dirname) {
  return fs.mkdirSync(dirname);
}

function generatorSubDirSync(rootFolderName) {
  const subDirs = ['__test__', '_story'];
  subDirs.forEach((subDir) => {
    generatorMkdirSync(subDir);
  });
}

function generatorComponent() {
  // const folders = await fg()
  const folderName = join(COMPONENT_ROOT, process.argv[2]);

  if (exist(folderName)) {
    throw new Error('该文件夹已存在');
  }

  generatorMkdirSync(folderName);
  generatorSubDirSync(folderName);
}

generatorComponent();
