### Git 能在特定的重要动作发生时触发自定义脚本

Git在执行```git init``` 之后会在```.git/hooks``` 目录生成一系列的钩子


+ #### 提交过程的钩子
    + ```pre-commit```: **键入**提交信息前运行;一般用来审核代码
    + ```prepare-commit-msg```: **启动** 提交信息编辑器之前,默认信息被**创建**之后运行;可以查看默认的提交信息,但不能查看提交信息编辑器里的信息
    + ```commit-msg```: 这个时候就可以查看信息编辑器里的信息(如果没有这里显示的是默认的提交信息)
    +  ```post-commit```: 整个提交过程**完成**之后运行,一般用于通知


```json
"devDependencies": {
    "@commitlint/cli": "^16.0.1", // 检测你的commit提交信息
    "commitizen": "^4.2.4", //提供git-cz的命令来代替我们的 commit命令 主要用于提交信息的提示
    "commitlint-config-cz": "^0.13.3", // 合并 cz-customizable 和 commitlint  配置
    "cz-customizable": "^6.3.0", // 自定义提交规范
    "lerna": "^4.0.0", // 多包管理工具
    "standard-version": "^9.3.2", // 版本发布日志
    "yorkie": "^2.0.0", // 针对git hook 钩子函数
    "eslint": "^8.6.0", // 代码检查工具
    "eslint-config-airbnb": "^19.0.4", // 采用airbnb配置
    "eslint-config-prettier": "^8.3.0",// 之前说了eslint也会检查代码的格式，这个插件就是关闭所有不必要或可能跟prettier产生冲突的规则 
    "eslint-plugin-import": "^2.25.4", // es6模块化的lint rule 配置
    "eslint-plugin-jsx-a11y": "^6.5.1", //jsx 的lint rule 配置
    "eslint-plugin-prettier": "^4.0.0",  //可以让eslint使用prettier规则进行检查，并使用--fix选项。像之前的格式不对时，eslint提示的红线
    "eslint-plugin-react": "^7.28.0", // reat的lint rule 配置
    "prettier": "^2.5.1", // 代码格式化工具
    "lint-staged": "^12.1.5", // 在你提交的文件中，执行自定义的指令
  }
```


### 关于在使用npm 和 yarn 之间的考量

+ **npm 问题** 
    + npm包版本遵循语义化版本,因此对于真实安装的包,在每个同事安装的版本不一致出现bug
    + npm在面对安装错误的时候,错误包的的错误信息占满整个控制台,以至于不知道是哪里发生错误
    + npm 安装包的时候 是 串行安装的,也就是等待一个包安装完之后在安装下一个包
    + .....
+ **yarn的解决**
    + 包的安装是并行安装,可见用yarn的速度比npm快了多,并且还支持并行安装
    + 简洁的输出日志

### yarn workspace
  + 作用
    + 能帮助你更好地管理多个子project的repo，这样你可以在每个子project里使用独立的package.json管理你的依赖，又不用分别进到每一个子project里去yarn
    + 保证所有子project的公共依赖只会被下载和安装一次
    + 所有子项目的的依赖只会在根目录上安装一个```node_modules```
    + 可以结合lerna 的 ```useWorkspaces``` 实现```lerna hoisting```
      + 其实就是对于公共的包,会只安装一次,且安装在根目录的```node_modules```,同时各个子项目的依赖也会有自己的```node_modules```
