module.exports = {
  // plugins: ['react'],

  // extends: ['airbnb', 'plugin:prettier/recommended'],
  root: true, // 表明当前文件夹下,内部没有指定eslint文件,就全部采用root 为 true的文件夹 为lint规则
  env: { // 指定环境
    browser: true,
    node: true,
    es6: true,
  },
  settings: {
    react: {
      "version": "detect"
    }
  },
  // 针对 js 和 ts  分别使用规则
  overrides: [
    {
      files: ['*.js', '*.jsx'],
      parser: '@babel/eslint-parser',
      extends: ["plugin:prettier/recommended"],
      plugins: ['react', 'react-hooks', 'import'],
      parserOptions: {
        requireConfigFile: false
      },
      rules: {
        'prettier/prettier': 'warn',
      }
    },
    {
      files: ['*.ts', '*.tsx'],
      extends: ['plugin:@typescript-eslint/recommended', 'plugin:import/typescript', 'plugin:react/recommended', "plugin:prettier/recommended"],
      parser: '@typescript-eslint/parser',
      parserOptions: {
        project: ['./tsconfig.eslint.json'],
      },
      plugins: ['react', 'react-hooks', 'import', '@typescript-eslint'],
      rules: {
        'prettier/prettier': 'warn',
      }
    }
  ]
};
