module.exports = {
  "stories": [
    '../packages/ui/**/_story/*.stories.(ts|tsx)',
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials"
  ],
  "framework": "@storybook/react"
}