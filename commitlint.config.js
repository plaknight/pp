// Level [0..2]: 0 disables the rule. For 1 it will be considered a warning for 2 an error.: 0关闭 1警告 2错误

module.exports = {
    extends: ['cz'],
    // commit 提交规则
    rules: {
        // must add these rules
        'type-empty': [2, 'never'], //https://commitlint.js.org/#/reference-rules
        'subject-empty': [2, 'never']
    }
};