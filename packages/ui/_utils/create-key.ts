// type Equal<X,Y> =

type characterMap = {
    q: 'Q';
    w: 'W';
    e: 'E';
    r: 'R';
    t: 'T';
    y: 'Y';
    u: 'U';
    i: 'I';
    o: 'O';
    p: 'P';
    a: 'A';
    s: 'S';
    d: 'D';
    f: 'F';
    g: 'G';
    h: 'H';
    j: 'J';
    k: 'K';
    l: 'L';
    z: 'Z';
    x: 'X';
    c: 'C';
    v: 'V';
    b: 'B';
    n: 'N';
    m: 'M';
};

type FirstToUpperCase<T extends string> = T extends `${infer A}${infer B}`
    ? A extends keyof characterMap
        ? `${characterMap[A]}${B}`
        : never
    : never;

export function createKey<P extends string, S extends string>(
    prefix: P,
    suffix: S
): S extends 'default' ? P : `${P}${FirstToUpperCase<S>}` {
    // 1 对于 S extends 'default' 这个条件来说  永远返回的参数 是 never 所以只返回`${P}${FirstToUpperCase<S>}`
    return (prefix +
        (suffix === 'default'
            ? ''
            : suffix.replace(/^[a-z]/, (startChar) => startChar.toUpperCase()))) as `${P}${FirstToUpperCase<S>}`;
}
