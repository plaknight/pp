import commonVariables from './_common';

const base = {
    // primary
    primaryHover: '#36ad6a',
    primaryDefault: '#18a058',
    primaryActive: '#0c7a43',
    primarySuppl: '#36ad6a',

    // info
    infoHover: '#4098fc',
    infoDefault: '#2080f0',
    infoActive: '#1060c9',
    infoSuppl: '#4098fc',

    // error
    errorHover: '#de576d',
    errorDefault: '#d03050',
    errorActive: '#ab1f3f',
    errorSuppl: '#de576d',

    // warning
    warningHover: '#fcb040',
    warningDefault: '#f0a020',
    warningActive: '#c97c10',
    warningSuppl: '#fcb040',

    // success
    successHover: '#36ad6a',
    successDefault: '#18a058',
    successActive: '#0c7a43',
    successSuppl: '#36ad6a',
};

const derived = {
    // ...base,
    ...commonVariables,
    // primary color
    primaryColor: base.primaryDefault,
    primaryColorHover: base.primaryHover,
    primaryColorPressed: base.primaryActive,

    borderColor: 'rgb(224, 224, 230)',
};

export type ThemeCommonVars = typeof derived;
export default derived;
