import commonVariables from './_common';
import { commonLight, ThemeCommonVars } from '../../_styles/common';

import { Theme } from '../../_minxins';

const self = (vars: ThemeCommonVars) => {
    const {
        borderRadius,
        fontSizeTiny,
        fontSizeSmall,
        fontSizeMedium,
        fontSizeLarge,
        heightTiny,
        heightSmall,
        heightMedium,
        heightLarge,
        borderColor,
        primaryColorHover,
        primaryColorPressed,
    } = vars;

    return {
        ...commonVariables,
        // ...vars,
        borderRadiusTiny: borderRadius,
        borderRadiusSmall: borderRadius,
        borderRadiusMedium: borderRadius,
        borderRadiusLarge: borderRadius,
        fontSizeTiny,
        fontSizeSmall,
        fontSizeMedium,
        fontSizeLarge,
        heightTiny,
        heightSmall,
        heightMedium,
        heightLarge,

        border: `1px solid ${borderColor}`,
        borderHover: `1px solid ${primaryColorHover}`,
        borderPressed: `1px solid ${primaryColorPressed}`,
        borderFocus: `1px solid ${primaryColorHover}`,
        borderDisabled: `1px solid ${borderColor}`,
    };
};

// 通过高阶类型
const buttonLight: Theme<'Button', ReturnType<typeof self>> = {
    name: 'Button', // 主题名词
    common: commonLight, // 主题基本样式
    self, // 该组件自有的样式
};

export default buttonLight;

export type ButtonTheme = typeof buttonLight;
