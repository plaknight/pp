import { c, cB, cE, cM, cNotM } from '../../_utils/cssr';
// vars:
// --n-bezier
// --n-bezier-ease-out
// --n-ripple-duration
// --n-opacity-disabled
// --n-text-color
// --n-text-color-hover
// --n-text-color-pressed
// --n-text-color-focus
// --n-text-color-disabled
// --n-color
// --n-color-hover
// --n-color-pressed
// --n-color-focus
// --n-color-disabled
// --n-border
// --n-border-hover
// --n-border-pressed
// --n-border-focus
// --n-border-disabled
// --n-ripple-color
// --n-border-radius
// --n-height
// --n-width
// --n-font-size
// --n-padding
// --n-icon-size
// --n-icon-margin
// --n-wave-opacity
// --n-font-weight

export default c([
    cB(
        'button',
        `
            font-weight: var(--n-font-weight);
            line-height: 1;
            font-family: inherit;
            padding: var(--n-padding);
            height: var(--n-height);
            font-size: var(--n-font-size);
            border-radius: var(--n-border-radius);
            color: var(--n-text-color);
            background-color: var(--n-color);
            width: var(--n-width);
            white-space: nowrap;
            outline: none;
            position: relative;
            z-index: auto;
            border: none;
            display: inline-flex;
            flex-wrap: nowrap;
            flex-shrink: 0;
            align-items: center;
            justify-content: center;
            user-select: none;
            text-align: center;
            cursor: pointer;
            text-decoration: none;
            transition:
            color .3s var(--n-bezier),
            background-color .3s var(--n-bezier),
            opacity .3s var(--n-bezier),
            border-color .3s var(--n-bezier);
        `,
        [
            cNotM('disabled', [
                c(
                    '&:focus',
                    {
                        backgroundColor: 'var(--n-color-focus)',
                        color: 'var(--n-text-color-focus)',
                    },
                    [
                        cE('state-border', {
                            border: 'var(--n-border-focus)',
                        }),
                    ]
                ),
                c(
                    '&:hover',
                    {
                        backgroundColor: 'var(--n-color-hover)',
                        color: 'var(--n-text-color-hover)',
                    },
                    [
                        cE('state-border', {
                            border: 'var(--n-border-hover)',
                        }),
                    ]
                ),
                c(
                    '&:active',
                    {
                        backgroundColor: 'var(--n-color-pressed)',
                        color: 'var(--n-text-color-pressed)',
                    },
                    [
                        cE('state-border', {
                            border: 'var(--n-border-pressed)',
                        }),
                    ]
                ),
                cM(
                    'pressed',
                    {
                        backgroundColor: 'var(--n-color-pressed)',
                        color: 'var(--n-text-color-pressed)',
                    },
                    [
                        cE('state-border', {
                            border: 'var(--n-border-pressed)',
                        }),
                    ]
                ),
            ]),
            cB(
                'base-wave',
                `
            pointer-events: none;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            animation-iteration-count: 1;
            animation-duration: var(--n-ripple-duration);
            animation-timing-function: var(--n-bezier-ease-out), var(--n-bezier-ease-out);
          `,
                [
                    cM('active', {
                        zIndex: 1,
                        animationName: 'button-wave-spread, button-wave-opacity',
                    }),
                ]
            ),
            cE(
                'border, state-border',
                `
            position: absolute;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            border-radius: inherit;
            transition: border-color .3s var(--n-bezier);
            pointer-events: none;
          `
            ),
            cE('border', {
                border: 'var(--n-border)',
            }),
            cE('state-border', {
                border: 'var(--n-border)',
                borderColor: '#0000',
                zIndex: 1,
            }),
        ]
    ),
]);
