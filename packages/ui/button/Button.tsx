import React, { useRef, useMemo, useState } from 'react';

import classNames from 'classnames';

import { buttonLight } from './styles';

import { useConfig, useTheme } from '../_minxins/index';
import { createKey } from '../_utils/create-key';

import style from './styles/button.cssr';

export type HTMLType = 'button' | 'reset' | 'submit';
export type Size = 'tiny' | 'small' | 'large' | 'medium';
export type Theme = 'solid' | 'borderless' | 'light';
export type Type = 'primary' | 'ghost' | 'dashed' | 'link' | 'text' | 'default';

export interface ButtonProps {
    color?: string;
    size?: Size;
    theme?: Theme;
    type?: Type;

    htmlType?: HTMLType;

    iconPosition?: 'left' | 'right';

    shape?: 'default' | 'circle' | 'round';

    // round?: boolean;
    bordered?: boolean;
    disabled?: boolean;

    loading?: boolean;

    onClick?: React.MouseEventHandler<HTMLButtonElement>;
    onMouseDown?: React.MouseEventHandler<HTMLButtonElement>;
    onMouseEnter?: React.MouseEventHandler<HTMLButtonElement>;
    onMouseLeave?: React.MouseEventHandler<HTMLButtonElement>;

    'aria-label'?: React.AriaAttributes['aria-label'];
}

const Border: React.FC<{ mergedClsPrefixRef: string }> = (props) => {
    const { mergedClsPrefixRef } = props;

    return (
        <>
            <div aria-hidden className={`${mergedClsPrefixRef}-button__border`}></div>
            <div aria-hidden className={`${mergedClsPrefixRef}-button__state-border`}></div>
        </>
    );
};

const Button: React.FC<ButtonProps> = (props) => {
    const {
        disabled = false,
        type = 'primary',
        color,
        size = 'medium',
        htmlType = 'button',
        loading = false,
        bordered = true,
        shape = 'default',
        onClick,
        onMouseDown,
        onMouseEnter,
        onMouseLeave,
    } = props;

    const theme = useTheme('Button', style, buttonLight);

    const mergedType = 'default';

    const { mergedClsPrefixRef } = useConfig(props);

    const cssVars = useMemo(() => {
        const {
            common: { cubicBezierEaseInOut, cubicBezierEaseOut },
            self,
        } = theme;

        // border
        let borderProps = {
            '--n-border': 'initial',
            '--n-border-hover': 'initial',
            '--n-border-pressed': 'initial',
            '--n-border-focus': 'initial',
            '--n-border-disabled': 'initial',
        };

        const {
            [createKey('height', size)]: height,
            [createKey('fontSize', size)]: fontSize,
            [createKey('padding', size)]: padding,
            [createKey('paddingRound', size)]: paddingRound,
            [createKey('iconSize', size)]: iconSize,
            [createKey('borderRadius', size)]: borderRadius,
            [createKey('iconMargin', size)]: iconMargin,
            // waveOpacity,
        } = self;

        if (type === 'text' || type === 'link') {
            // 如果是 text 和 link 类型 不需要边框样式
            borderProps = {
                '--n-border': 'none',
                '--n-border-hover': 'none',
                '--n-border-pressed': 'none',
                '--n-border-focus': 'none',
                '--n-border-disabled': 'none',
            };
        } else {
            // console.log(createKey('border', mergedType))

            borderProps = {
                '--n-border': self[createKey('border', mergedType)],
                '--n-border-hover': self[createKey('borderHover', mergedType)],
                '--n-border-pressed': self[createKey('borderPressed', mergedType)],
                '--n-border-focus': self[createKey('borderFocus', mergedType)],
                '--n-border-disabled': self[createKey('borderDisabled', mergedType)],
            };
        }

        const sizeProps = {
            '--n-width': shape === 'circle' && type !== 'text' ? height : 'initial',
            '--n-height': type === 'text' ? 'initial' : height,
            '--n-font-size': fontSize,
            '--n-padding':
                shape === 'circle'
                    ? 'initial'
                    : type === 'text'
                    ? 'initial'
                    : shape === 'round'
                    ? paddingRound
                    : padding,
            '--n-icon-size': iconSize,
            '--n-icon-margin': iconMargin,
            '--n-border-radius':
                type === 'text' ? 'initial' : shape === 'circle' || shape === 'round' ? height : borderRadius,
        };
        return {
            '--n-bezier': cubicBezierEaseInOut,
            '--n-bezier-ease-out': cubicBezierEaseOut,
            ...sizeProps,
            ...borderProps,
        };
    }, []);

    const buttonClassName = classNames([
        `${mergedClsPrefixRef}-button`,
        `${mergedClsPrefixRef}-button--${type}-type`,
        { [`${mergedClsPrefixRef}-button--disabled`]: disabled },
        { [`${mergedClsPrefixRef}-button--color`]: color },
        { [`${mergedClsPrefixRef}-button--loading`]: loading },
    ]);

    const eventProps = { onClick, onMouseDown, onMouseEnter, onMouseLeave };

    const baseProps = { type: htmlType, className: buttonClassName, style: cssVars as React.CSSProperties, disabled };

    return (
        <button {...eventProps} {...baseProps}>
            <span className={`${mergedClsPrefixRef}-button__content`}>{props.children}</span>
            {/* 模拟普通边框 */}
            {bordered ? <Border mergedClsPrefixRef={mergedClsPrefixRef} /> : null}
        </button>
    );
};

export default Button;
