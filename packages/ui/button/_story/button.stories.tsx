import * as React from 'react';

import { storiesOf } from '@storybook/react';
import Button from '../Button';

const stories = storiesOf('Button', module);

stories.add('base', () => <Button>123213</Button>);
