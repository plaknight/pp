import React, { useEffect, useMemo } from 'react';
import { CNode } from 'css-render';
import { merge } from 'lodash';

import globalStyle from '../_styles/global/index.cssr';
import { cssrAnchorMetaName } from './vars';

import { ThemeCommonVars } from '../_styles/common';

export interface Theme<N, T> {
    name: N;
    common?: ThemeCommonVars;
    // peers: R;
    self: (vars: ThemeCommonVars) => T;
}

function useTheme<N, T>(mountId: string, style: CNode | undefined, theme: Theme<N, T>) {
    //组件调用时执行一次
    useEffect(() => {
        // 挂载组件的样式
        style.mount({
            id: mountId,
            head: true,
            anchorMetaName: cssrAnchorMetaName,
        });

        // 挂载全局的样式
        globalStyle.mount({
            id: 'pp-ui/global', // style 标签 cssr-id
            head: false,
            anchorMetaName: cssrAnchorMetaName,
        });
    }, []);

    const themeRef = useMemo(() => {
        return {
            ...theme,
            self: theme.self(theme.common),
        };
    }, []);

    return themeRef;
}

export default useTheme;
