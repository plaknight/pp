import { defaultClsPrefix } from './vars';

type Config = Readonly<{
    [key: string]: unknown;
}>;

function useConfig(config: React.PropsWithChildren<any>) {
    return {
        mergedClsPrefixRef: defaultClsPrefix,
    };
}

export default useConfig;
