export { default as useConfig } from './use-config';
export { default as useTheme } from './use-theme';

export type { Theme } from './use-theme';

export { defaultClsPrefix } from './vars';
