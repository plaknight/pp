import React from 'react';
import ConfigContext, { ContextValue } from './context';

import { defaultClsPrefix } from '../_minxins/index';

export type ConfigProviderProps = ContextValue;

const ConfigProvider: React.FC<ConfigProviderProps> = (props) => {
    const { children, ...reset } = props;

    return (
        <ConfigContext.Provider value={{ ...reset }}>
            <div className={`${defaultClsPrefix}-config-provider`}>{children}</div>
        </ConfigContext.Provider>
    );
};

export default ConfigProvider;
