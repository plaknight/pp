import React from 'react';
// import { Locale } from '../locale/interface';

export interface ContextValue {
    theme?: any;
}

const ConfigContext = React.createContext<ContextValue>({});

export default ConfigContext;
